# Updating

## Updating things from other projects

- jQuery: Download the __compressed, production__ version from [here](https://jquery.com/download/) and place in `public/assets/js/jquery.min.js`
- Papa Parse: Download the latest stable version from [here](https://github.com/mholt/PapaParse/releases) and place the `papaparse.min.js` to `public/assets/js/papaparse.min.js`
- Plyr: Download Plyr from [here](https://github.com/sampotts/plyr/releases) and keep the contents of `dist` folder to `public/assets/js/plyr`
- SolaimanLipi: Download the latest version from [here](http://www.ekushey.org/index.php/page/solaimanlipi) and put it in `public/assets/fonts` and delete the old one. Then provide the new filename in the `public/assets/css/font.css`.


## Updating data

The data is usually put in `.csv` files in `public/data`. By default it loads the data from `ajob-data.csv`. See `public/player.html` around line 170. Additional .csv files can be created and accessed with the `list` GET parameter (see below).


# GET parameters

- order: reverse|shuffle
- tags: (tag name)|(tag name 1),(tag name 2)
- list: (csv name without ".csv" extension)

Some examples:

- `player.html` - will load all the videos in `ajob-data.csv`
- `player.html?order=reverse` - will load the videos in reverse order from `ajob-data.csv`
- `player.html?order=shuffle` - will load the videos in random/shuffled order from `ajob-data.csv`
- `player.html?tags=night` - will load the videos that has the tag `night` from `ajob-data.csv`
- `player.html?tags=night,relaxing&order=shuffle` - will load the videos that has the tag `night` and `relaxing` in random order from `ajob-data.csv`
