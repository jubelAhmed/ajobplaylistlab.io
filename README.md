![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

# Ajob Desher Playlist (আজব দেশের প্লেলিস্ট)

If the title confuses you, the words are from Bengali/Bangla language. I am from Bangladesh, so I used these words. But you can fork it and give it a different name. "Ajob" means strange, weird, uncommon. "Desher" means relating to some country. So Ajob Desher Playlist means **Playlist of Uncommon Countries**.

It plays songs from different countries to you and optionally filters out the mood of songs you want to listen to from the playlist data.


# How do I listen to this playlist?

Just go to [ajobplaylist.gitlab.io](https://ajobplaylist.gitlab.io) and hit the play button at the top or any other play button.

You can find the playlist CSV data in `public/data` folder. There are URL GET parameters you can check for in `DEVELOPING.md`.


# How to run offline?

If you want to run offline on a localhost environment, you can do so. Just download or pull this repository and put on your webroot somewhere and use. It is just static HTML/CSS/JS. So it should work on any minimal server setup (vanilla Apache/nginx etc.)

If you don't have a server setup and setting up servers and configurations seems hard, there is an easy way. Install [nodejs](https://nodejs.org/en/download/) and install [simple-server](https://www.npmjs.com/package/simple-server):

```
sudo npm install --global simple-server
```

_* If you are using Windows, you will need to ommit `sudo` from these commands._

When you want to use it, just `cd` to the repo dir and run:

```
sudo simple-server public 3300
```

Here, `public` is your webroot, where the servable files are and `3300` is the port number. Then open `localhost:3300` on your browser.


# License

This project uses different open source projects. So licenses to those will apply from their respective projects. The songs are not created by me and I do not own their copyright. The rights belong to their respective copyright holder(s). They are just streamed from their source websites. If you feel anything infringes copyright or you do not wish to see your song listed here, raise a [public issue](https://gitlab.com/ajobplaylist/ajobplaylist.gitlab.io/issues) or contact at: ajobplaylist<span>@</span>protonmail<span>.</span>com.

The code specifically to this project is licensed under MIT. It can be used for commercial or non-commercial purpose.
