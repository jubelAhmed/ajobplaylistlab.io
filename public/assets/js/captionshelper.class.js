CaptionsHelper = function() {
	this.itemData=null;
	this.captionDuration=5000;

	// Should be run every second the item plays
	// Data should have:
	// - current
	// - duration
	// - remaining
	this.secondTick = function(eventData) {
		if (eventData.current == 5) {
			// A random id of 6 digits is given to each captions
			var captionId=parseInt(Math.random()*999999);
			var ratingText=parseInt(this.itemData[COL_RATING])>6 ? '♥' : '☺️';
			$(document).trigger('captionsHShowCaption',{
				id: captionId,
				captionText: ('Song from ' + this.itemData[COL_COUNTRY] + '...' + ratingText),
				duration: this.captionDuration
			});
			setTimeout(function(){
				$(document).trigger('captionsHHideCaption',{
					id: captionId
				});
			}, this.captionDuration);
			// TODO: Implement cases for seeking
		}
	}
}