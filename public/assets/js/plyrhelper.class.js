PlyrHelper = function() {
	this.plyrContainerElement='.player-container';
	this.plyrElementID='player';
	this.prevSecond=null;
	// Variable to hold Plyr instance
	var player=null;

	// Check if jQuery is included
	if (!jQuery) {
		throw "jQuery is not declared";
	}
	// Check if Plyr is included
	if (!Plyr) {
		throw "Plyr is not declared";
	}

	// Gets YouTube video id from the full URL
	this.getVideoId = function(url) {
		// source: https://stackoverflow.com/a/8260383
		// TODO: Add other video providers as well, such as Vimeo.
		var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
		var match = url.match(regExp);
		return (match&&match[7].length==11)? match[7] : false;
	}

	// Gets the HTML necessary to show the Plyr player later on
	this.getPlyrHtml = function(videoUrl) {
		var videoId=this.getVideoId(videoUrl);
		// TODO: Adopt to other video providers as well, such as Vimeo.
		return '			<div class="plyr__video-embed" id="player"> \
		<iframe src="https://www.youtube.com/embed/'+videoId+'?origin=https://plyr.io&amp;iv_load_policy=3&amp;modestbranding=1&amp;playsinline=1&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1" \
			allowfullscreen \
			allowtransparency \
			allow="autoplay" \
		></iframe> \
	</div>';
	}

	this.changeVideo = function(videoUrl) {
		if (this.player==null) {
			this.initiatePlayer(videoUrl);
		} else {
			this.player.source = {
				type: 'video',
				sources: [
					{
						src: this.getVideoId(videoUrl),
						provider: 'youtube',
					},
				],
			};
		}
	}

	this.initiatePlayer = function(initialUrl) {
		// Get the fallback code
		$(this.plyrContainerElement).html(this.getPlyrHtml(initialUrl));

		// sources:
		// https://stackoverflow.com/a/53682741
		// https://github.com/sampotts/plyr/blob/master/controls.md
		const controls = [
			'rewind',
			'play',
			'fast-forward',
			'progress',
			'current-time',
			'duration',
			'mute',
			'volume',
			'captions',
			'fullscreen'
		];
		this.player = new Plyr(document.getElementById(this.plyrElementID), {
			autoplay: true,
			controls: controls,
			settings: ['captions', 'quality', 'speed', 'loop']
		});

		this.player.on('ready', event => {
			// Detach events set by the stubborn listeners.js
			// source: https://stackoverflow.com/a/39026635
			document.querySelector("button[data-plyr='rewind']").outerHTML = document.querySelector("button[data-plyr='rewind']").outerHTML;
			document.querySelector("button[data-plyr='fast-forward']").outerHTML = document.querySelector("button[data-plyr='fast-forward']").outerHTML;

			$("button[data-plyr='rewind']").on( "click", function(){
				$(document).trigger('plyrHPrevButtonClicked');
			});
			$("button[data-plyr='fast-forward']").on( "click", function(){
				$(document).trigger('plyrHNextButtonClicked');
			});

			$(document).trigger('plyrHReady');
		});

		this.player.on('timeupdate', event => {
			var second=parseInt(event.detail.plyr.currentTime);

			if (second!=this.prevSecond) {
				// Raise event once a second
				$(document).trigger('playlistHSecondTick',{
					current: second,
					duration: parseInt(event.detail.plyr.duration),
					remaining: parseInt(event.detail.plyr.duration - second)
				});
				this.prevSecond=second;
			}
		});

		this.player.on('ended', event => {
			$(document).trigger('plyrHVideoEnded');
		});
	}
}