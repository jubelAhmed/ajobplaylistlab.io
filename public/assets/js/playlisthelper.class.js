PlaylistHelper = function() {
	this.playlistData=null;
	this.filteredPlaylistData=new Array;
	this.currentIndex=null; // zero based
	this.playlistTotalItems=null; // zero based, total after filtering
	//
	this.getParams=null; // GET request variables (would be obsolete)
	//
	this.hasFilter=false; // True if any filter is set
	this.filterParams=null; // GET request variables (would be obsolete)
	this.filterSeparator=','; // The separator of filters in get parameter
	this.filterTags=null; // Filter tags data array
	this.filterRating=null; // Filter tags data array
	//
	this.hasMod=false; // True if any filter is set
	this.modOrder=null; // Whether to reverse the playlist data

	if (!jQuery) {
		throw "JQuery is not declared";
	}

	// Load playlist based on some filename or id
	this.loadPlaylist = function(filename, autoplay=true) {
		this.loadCSVPlaylist(filename, autoplay);
	}

	// Load playlist data from CSV
	// Just a separate function to keep things clean and
	// in case other format implementations are needed in future
	this.loadCSVPlaylist = function(filename, autoplay=true) {
		fetch(filename)
			.then(response => response.text())
			.then(text => {
				// Fetch and prepare data
				this.playlistData = Papa.parse(text, {
					// To ignore last empty line left in csv
					skipEmptyLines: 'greedy'
				}).data;
				// Fill the filtered array with all items/data
				this.filteredPlaylistData=[];
				for (i=0; i<this.playlistData.length; i++) {
					this.filteredPlaylistData[i]=i;
				}
				console.log(this.playlistData);
				console.log(this.playlistData[0][COL_VIDEO_URL]);
				this.currentIndex=0;

				// Apply filters and mods
				this.applyParams();
				this.playlistTotalItems=this.filteredPlaylistData.length;

				// Trigger events
				$(document).trigger('playlistHPlaylistLoaded');
				if (autoplay==true) {
					$(document).trigger('playlistHItemChanged',{
						itemIndex: this.currentIndex,
						itemURL: this.getItemWithIndex(this.currentIndex)
					});
				}
				})
	}

	// Gets GET request parameter
	// Source: https://stackoverflow.com/a/831060
	// TODO: Move to a common .js file
	this.getParam = function(name,url=null){
		if (url==null)
			url=location.search;
		if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(url))
			return decodeURIComponent(name[1]);
	}

	// Set filter get parameter
	this.setParams = function(filterString=null) {
		//var filterText = this.getParam('filter');
		var filterTags = this.getParam('tags', filterString);
		this.filterTags = filterTags==null ? null : filterTags.split(this.filterSeparator);
		this.filterRating = this.getParam('rating', filterString);
		this.hasFilter = (this.filterTags || this.filterRating) ? true : false;

		this.modOrder = this.getParam('order', filterString);
		this.hasMod = (this.modOrder) ? true : false;
	}

	// Applies all the parameters to the playlist
	// Filters, shuffle etc.
	this.applyParams = function() {
		// Filter
		this.applyFilters();
		this.applyMods();
	}

	// Apply filters to the list items
	this.applyFilters = function() {
		// TODO: implement a way so that original list array stays intact
		if (this.hasFilter) {
			// Loop through the list in reverse so that when an item is removed
			// we don't encounter an empty item.
			// Source: https://stackoverflow.com/a/28122081
			for (var i = this.playlistData.length - 1; i >= 0; i--) {
				if (this.checkItemForFilters(i) == false) {
					this.filteredPlaylistData.splice(i, 1);
				}
			}
		}
	}

	// Apply modifications to playlist based on parameters
	// e.g. reverse order
	this.applyMods = function() {
		if (this.hasMod) {
			if (this.modOrder=='reverse') {
				this.filteredPlaylistData.reverse();
			} else if (this.modOrder=='shuffle') {
				this.filteredPlaylistData.shuffle();
			}
		}
	}

	// Adds shuffle functionality to arrays
	// Source: https://www.kirupa.com/html5/shuffling_array_js.htm
	Array.prototype.shuffle = function() {
		let input = this;

		for (let i = input.length-1; i >=0; i--) {

			let randomIndex = Math.floor(Math.random()*(i+1));
			let itemAtIndex = input[randomIndex];

			input[randomIndex] = input[i];
			input[i] = itemAtIndex;
		}
		return input;
	}

	// Check whether the item should pass by the filter
	this.checkItemForFilters = function(itemIndex) {
		// If there are no filters set, don't bother checking and allow the item
		if (this.hasFilter==false) {
			return true;
		}
		var pass = false;
		if (this.filterTags) {
			for (var i = 0; i < this.filterTags.length; i++) {
				if (isNaN(this.filterTags[i]) == false && this.filterTags[i].length == 4) {
					// Allow items with specific year
					if (this.playlistData[itemIndex][COL_LISTING_DATE].indexOf(this.filterTags[i]) >= 0) {
						pass=true;
						continue;
					}
				} else if (this.filterTags[i] == 'kpop') {
					// Allow items from the kpop country
					if (this.playlistData[itemIndex][COL_COUNTRY].indexOf('South Korea') >= 0) {
						pass=true;
						continue;
					}
				} else {
					// Check other keywords in the subplaylist column
					if (this.playlistData[itemIndex][COL_SUBPLAYLIST].indexOf(this.filterTags[i]) >= 0) {
						pass=true;
						continue;
					} else {
						// If the keyword is missing, we won't allow the item (will break out of loop)
						pass=false;
					}
				}
				// If one of the filter/tags are not found, we won't allow the item
				if (pass==false) {
					break;
				}
			}
		} else {
			// No tag filter is defined, so we allow all
			pass=true;
		}
		if (this.filterRating && pass==true) {
			if (this.playlistData[itemIndex][COL_RATING] == this.filterRating) {
				pass=true;
			} else {
				pass=false;
			}
		//} else {
			// No rating filter is defined, so we allow all
			//pass=true;
		}
		return pass;
	}

	this.getCurrentItemData = function() {
		return this.playlistData[ this.filteredPlaylistData[this.currentIndex] ];
	}

	// Gets video URL for the first item
	this.getCurrentIndex = function() {
		return this.currentIndex;
	}

	// Gets video URL for the first item
	this.getFirstItem = function() {
		return this.getItemWithIndex(0);
	}

	// Gets the total items on the playlist
	// (after filtering)
	this.getTotalItems = function() {
		return this.playlistTotalItems;
	}

	// Gets playlist item data with the index number
	this.getItemWithIndex = function(index) {
		return this.playlistData[ this.filteredPlaylistData[index] ][COL_VIDEO_URL];
	}

	this.getVideoThumbnail = function(videoUrl) {
		// TODO: Find a better way to reference common functions
		player = new PlyrHelper();
		var videoId=player.getVideoId(videoUrl);
		return '<img class="thumbnail" src="https://img.youtube.com/vi/'+videoId+'/default.jpg" />';
	}

	// Output HTML for the playlist
	this.renderPlaylist = function() {
		var output = '<ul class="playlist-items">';
		var videoId;
		var i;
		var indexInPlaylistData;
		//for (i=0; i<this.playlistData.length-1; i++) {
		for (i=0; i<this.playlistTotalItems; i++) {
			indexInPlaylistData = this.filteredPlaylistData[i];
			output += '<li class="playlist-item item-'+i+'" data-play-index="'+i+'" title="'+this.playlistData[indexInPlaylistData][COL_VIDEO_TITLE]+'">' + this.getVideoThumbnail(this.playlistData[indexInPlaylistData][COL_VIDEO_URL]) + '<span class="video-country">'+this.playlistData[indexInPlaylistData][COL_COUNTRY] + '</span> <span class="video-title">' + this.playlistData[indexInPlaylistData][COL_VIDEO_TITLE] + '</span></li>';
		}
		output += '</ul>';
		return output;
	}

	// Go to next item on the playlist
	this.nextItem = function() {
		if (this.playlistData != null) {
			if (this.currentIndex != this.playlistTotalItems-1) {
				this.currentIndex++;
				$(document).trigger('playlistHItemChanged',{
					itemIndex: this.currentIndex,
					itemURL: this.getItemWithIndex(this.currentIndex)
				});
			}
		}
	}

	// Go to previous item on the playlist
	this.prevItem = function() {
		if (this.playlistData != null) {
			if (this.currentIndex != 0) {
				this.currentIndex--;
				$(document).trigger('playlistHItemChanged',{
					itemIndex: this.currentIndex,
					itemURL: this.getItemWithIndex(this.currentIndex)
				});
			}
		}
	}

	// Go to the item with certain index number
	this.playItemWithIndex = function(index) {
		this.currentIndex=index;
		console.log(this.getItemWithIndex(this.currentIndex));
		$(document).trigger('playlistHItemChanged',{
			itemIndex: this.currentIndex,
			itemURL: this.getItemWithIndex(this.currentIndex)
		});
	}
}